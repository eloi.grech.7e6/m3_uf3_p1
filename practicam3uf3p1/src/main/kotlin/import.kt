import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.nio.file.Path

// busca fitxers en la carpeta import i els processa
fun importarUsuaris(ruta:Path) {

    //la carpeta on buscarem fitxers
    val import = ruta.resolve("import/")

    val nFiles = contarFitxes(import)
    if (nFiles>0) {
        println("IMPORT FILES FOUND, proceding to import $nFiles files\n")

        //el fitxer de resultat
        val userDataFile = ruta.resolve("data/userData.txt").toFile()
        if (!userDataFile.exists()) {
            //si no existeix es crea
            userDataFile.createNewFile()
        }

        val fitxers = ls(import)
        for (fitxer in fitxers) {
            //importem cada fitxer
            importFromOldFile(userDataFile,fitxer)

            //i despres l'eliminem
            eliminarFitxer(fitxer)
        }
    }
}

//reb un fitxer anticuat i processa les dades guardantles al fitxer actual
fun importFromOldFile(newFile:File,oldFile: Path){
    println("\t${oldFile.fileName}\n\t\tIMPORTING DATA ")

    val oldContent = llegirFitxer( oldFile )
    val oldUsuarios = oldContent.split(";")

    val usuarios = mutableListOf<String>()

    val lastID = getLastID(newFile)

    for (user in oldUsuarios){
        //actualitzem cada usuari
        val newUser = actualizarUsuario(user,usuarios.size+lastID +1)

        //no afegir els No actius i bloquejats
        if(newUser[4].toBoolean() || !newUser[5].toBoolean()){
            usuarios.add(newUser.joinToString(";"))
        }
    }

    val newContent = usuarios.joinToString("\n")
    //i s'afageix el contingut
    newFile.appendText(newContent)

    println("\t\tIMPORT CORRECT; proceding to delete\n")

}




fun getLastID(file: File):Int {
    val input = BufferedReader(FileReader(file))
    var resultado = "0" //return 0 if is empty

    while (input.ready()){
        val linea = input.readLine()
        if (linea != "") {
            resultado=linea
        }
    }
    val id = resultado.split(';')[0]

    return id.toInt()
}



fun actualizarUsuario(antiguo:String,id:Int):List<String>{
    val campos = antiguo.split(",")
    val newCampos = mutableListOf<String>()
    for(c in campos){
        newCampos.add(c)
    }
    newCampos[0] = id.toString()
    return newCampos
}


