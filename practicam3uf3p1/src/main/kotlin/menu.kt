import java.nio.file.Path

// opera un bucle i mentre el usuari bo vulgui sortir aqui es queda
fun menu(ruta:Path) {

    //opcions del menu
    val items = listOf<String>(
        "Modificar usuari",
        "Consultar usuaris",
        "Crear usuari",
        "Des/bloquejar usuari",
        "Llegir Usuaris",
        "Fer Backup"
    )

    var sortir = false
    do {
        //cls()
        println("MENU:")
        for (i in 0 until items.size) {
            val item = items[i]
            println("\t${i+1} - $item")
        }
        println("\t0 - Sortir")
        print("Introduce una opción: ")

        val userInput = leerUserRango(0, items.size)

        //les functions corresponents a cada menu
        when (userInput) {
            1-> modificarUsuari(ruta)
            2-> consultarUsuaris(ruta)
            3-> crearUsuari(ruta)
            4-> des_bloquejarUsuari(ruta)
            5-> importarUsuaris(ruta)
            6-> ferBackups(ruta)
            0-> sortir = true
        }
    } while (!sortir)
}


//demanar per consola un valor dins de un rang
fun leerUserRango(min: Int, max: Int): Int {
    var userInput = readln().toInt()

    while (userInput < min || userInput > max){
        println("ERROR, el valor tiene que estar entre $min y $max")
        userInput = readln().toInt()
    }
    return userInput
}

//netejar pantalla
fun cls(){
    println("\n".repeat(50))
}

