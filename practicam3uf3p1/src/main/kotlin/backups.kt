import java.nio.file.Path
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


fun ferBackups(ruta:Path) {
    //el contingut del backup es el de userData.txt
    val content = llegirFitxer( ruta.resolve("data/userData.txt") )

    //el nom del arxiu
    val currentDate = LocalDateTime.now()
    val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val dateFormatted = currentDate.format(dateFormatter)
    val filename = dateFormatted + "userData.txt"


    val path = ruta.resolve("backup/")
    val file = path.resolve(filename).toFile()
    //si existeix sobrescriu sino el crea
    if (file.exists()){
        file.writeText(content)
    }else {
        crearFitxer(file, content)
    }

    println("BACKUP done ${file.name}")
}