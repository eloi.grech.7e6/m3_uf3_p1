import java.io.File
import java.nio.file.Path
import kotlin.io.path.createDirectory
import kotlin.io.path.exists
import kotlin.io.path.listDirectoryEntries

//retorna el numero de fitxers dins una carpeta
fun contarFitxes(path: Path): Int {
    if (path.exists()){
        val files = path.listDirectoryEntries()
        return files.size
    }
    return 0
}

//retorna una llista dels fitxers dins una carpeta
fun ls(path:Path): List<Path> {
    return path.listDirectoryEntries()
}

//elimina un fitxer
fun eliminarFitxer(path: Path): Boolean {
    val file = path.toFile()
    if (file.exists()) {
        val correcto = file.delete()
        return correcto
    }
    return false
}

//retorna el contingut de un fitxer en un string
fun llegirFitxer(path:Path): String {
    val file = path.toFile()
    var result = ""
    file.forEachLine {
        result+=it+"\n"
    }
    return result
}

//crea un fitxer
fun crearFitxer(file: File, content: String ="") {
    if (!file.exists()){
        val correcto = file.createNewFile()
        if(correcto){
            file.writeText(content)
        }
        //println( if (correcto) "CORRECTO" else "MAL")
    }
}

//si el fitxer no existeix el crea, i retorna el fitxer
fun getFileOrCreateEmpty(path:String):File{
    val file = File(path)
    if(!file.exists()){
        //may throw error if not have permision
        file.createNewFile()
    }
    return file
}

//asegura que existe una carpeta, sino, la crea
fun ensureFolder(ruta: Path, folderName:String):Path{
    val folder = ruta.resolve(folderName)
    if( !folder.exists() ){
        folder.createDirectory()
    }
    return folder
}