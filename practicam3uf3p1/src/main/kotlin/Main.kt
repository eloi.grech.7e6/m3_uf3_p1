import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.createDirectory
import kotlin.io.path.exists

fun main(args: Array<String>) {
    val ruta = Path("./src/main/kotlin/")


    ensureFolder(ruta,"import/")
    ensureFolder(ruta,"backup/")
    ensureFolder(ruta,"data/")


    //ON START
    importarUsuaris(ruta)

    menu(ruta)

    //ON EXIT
    ferBackups(ruta)

}



