import java.nio.file.Path
import kotlin.io.path.readLines
import kotlin.io.path.writeText

//demana al client i crea un usuari
fun crearUsuari(ruta:Path):String {
    val lastID = getLastID(ruta.resolve("data/userData.txt").toFile())
    val id = lastID+1

    println("Introdueix el nom de l'usuari:")
    val name = readln()
    println("Introdueix el número de telèfon:")
    val phoneNumber = readln()
    println("Introdueix el correu electrònic:")
    val email = readln()

    val active = true
    val blocked = false

    val dadesAfegir = "$id;$name;$phoneNumber;$email;$active;$blocked"
    afegirUsuariFitxer(ruta,dadesAfegir)
    return dadesAfegir
}


//demana les dades al client per modificar un usuari
fun modificarUsuari(ruta: Path) {

    //farem us de una funcio lambda que sera la encarregada de modificar les dades
    fun modUserFields(campos:List<String>):List<String>{
        val newCampos = mutableListOf<String>()
        for(c in campos){
            //create a copy
            newCampos.add(c)
        }

        println("Introdueix el nou nom:")
        newCampos[1] = readln()
        println("Introdueix el nou telèfon:")
        newCampos[2] = readln()
        println("Introdueix el nou email:")
        newCampos[3] = readln()

        return newCampos
    }
    modUser(ruta,::modUserFields)
}

//demana la id de usuari i modifica el valor de bloquejat
fun des_bloquejarUsuari(ruta:Path) {

    //farem us de una funcio lambda que sera la encarregada de modificar les dades
    fun modAdmin(campos:List<String>):List<String>{
        val newCampos = mutableListOf<String>()
        for(c in campos){
            newCampos.add(c)
        }

        val bloq = !campos[5].toBoolean()
        newCampos[5] = bloq.toString()
        if(bloq){
            println("L'usuari ${campos[0]} ha sigut BLOQUEJAT")
        }else{
            println("L'usuari ${campos[0]} ha sigut DESBLOQUEJAT")
        }
        return newCampos
    }
    modUser(ruta,::modAdmin)
}

//reb la funcio lambda tant de modificarUsuari() com de des_bloquejarUsuari()
fun modUser(ruta:Path,lambda:(List<String>)->List<String>){
    val fitxer = ruta.resolve("data/userData.txt")
    val usuaris = fitxer.readLines().toMutableList()

    //demana les dades
    println("Introdueix la ID de l'usuari a modificar:")
    val idUsuariModificar = readln().toInt()

    var found = false
    for (user in usuaris.indices){
        val campos = usuaris[user].split(";")
        if (campos[0].toInt()==idUsuariModificar){
            //l'ha trobat

            println("MODIFIN USER:\n\t${usuaris[user]}")
            found = true

            //executa la lambda
            val newCampos = lambda(campos)
            usuaris[user]= newCampos.joinToString(";")
            break
        }
    }

    //si no ha trobat cap usuari coincident amb la id
    if(!found){
        println("No user found with the given ID $idUsuariModificar")
        return
    }

    //guarda les dades modificades
    val newContent = usuaris.joinToString("\n")
    fitxer.writeText(newContent)
}

//afegeix un usuari al final del arxiu
fun afegirUsuariFitxer(ruta:Path,dades: String) {
    //TODO comprobar que la ultima linea no es linea vuida
    val file = ruta.resolve("data/userData.txt").toFile()
    file.appendText("\n"+dades)
}

//imprimeix els usuaris per consola
fun consultarUsuaris(ruta:Path){
    val usuaris = llegirFitxer( ruta.resolve("data/userData.txt") )
    println(usuaris)
}
